#!/usr/bin/python
# -*- coding: utf-8 -*-

import psycopg2


def connect():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...\n')
        conn = psycopg2.connect(host="localhost", database="DBS_Assignment/Project", user="postgres", password="*******")

        # create a cursor
        mycursor = conn.cursor()

        # execute a statement directly
#        country = input("Please enter the Countryname you want to get the info:\n")
#        mycursor.execute(f"SELECT * FROM life_info WHERE country_name= '{country}'")
        # display the first entry for selected country in the  Schema from database
#        result = mycursor.fetchone()
#        print(result)
        # Result and Status of the first entry for the selected country
#        print('The name of the Country is ' + str(result[0]) + '. The status is: ' + result[2] + '\n')
#        print("\n")
       
       # print the information of the country regarding the life expectancy
#        print(f"information of {country} regarding the life expectancy: ")

#        statement1 = f"SELECT * FROM life_info WHERE country_name ='{country}'"
#        mycursor.execute(statement1)
#        result1= mycursor.fetchall()
#        for x in result1:
#            print(x)
#        print("\n")

#        # get the total population and the co2-emission of the country
#        print(f"total population and the co2-emission of {country}: ")
#        statement2 = f"SELECT p.country_name, p.total_count, c.annual_co2_emission  FROM population_total p, co2_emission c WHERE p.country_name = c.country_name AND p.year=c.year AND p.country_name='{country}' AND p.year=2015"
#        mycursor.execute(statement2)
#        result2 = mycursor.fetchall()
#       print(result2)
#        print("\n")


        # let's get real and plot

        # import packages
        import pandas as pd
        import pymysql
        import seaborn as sns
        import matplotlib.pyplot as plt

# plot a barplot
        # interactive year selection
        year = input("Please enter the year you want to get the plot for:\n")

        # define the SQL query
        SQL_Query = pd.read_sql_query(f"SELECT country_name, code, year, annual_co2_emission FROM co2_emission WHERE year = {year} ", conn)

        # declair list as dataframe
        df1 = pd.DataFrame(SQL_Query, columns=['country_name', 'code', 'year', 'annual_co2_emission'])
        # some basic information
        print(df1.head())
        print(df1.shape)
        print(df1.dtypes)
        print(df1[df1.isnull()].count()) 
        print(df1[df1.duplicated()].count())
        
        # delete world from dataframe (as it would alter the scale drastically)
        df= df1[df1.country_name != 'World']

        # sorting regarding co2_emission
        df1_sort1 = df.sort_values('annual_co2_emission',ascending=False)
        print(df1_sort1)

        # plotting the barplot 
        sns.catplot(x="country_name", y="annual_co2_emission", kind="bar", data=df1_sort1.head(10),height=5, aspect=27/9,)
        plt.show()

# plot a linear regression (co2_emission and life_expectancy)
        import numpy as np

        # interactive counry selection
        country = input("Please enter the country you want to get the plot for:\n")
        # define the SQL query
        SQL_Query1 = pd.read_sql_query(f"SELECT c.country_name, c.year, annual_co2_emission, life_expectancy FROM co2_emission c, life_info l WHERE c.country_name=l.country_name AND c.year=l.year AND c.country_name= '{country}' ", conn)
        # declair list as dataframe
        df2 = pd.DataFrame(SQL_Query1, columns=['country_name', 'year', 'annual_co2_emission', 'life_expectancy'])
        print(df2.head())

        # plot regression 
        sns.regplot(x="annual_co2_emission", y="life_expectancy", data=df2);
        plt.show()


        # close the communication with the PostgreSQL
        mycursor.close()
    except (Exception, psycopg2.DatabaseError) as error:
     print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')


def main():
    connect()

if __name__ == '__main__':
    main()
