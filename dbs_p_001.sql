CREATE SCHEMA dbs_p;

CREATE TABLE dbs_p.country (
	_country_name varchar(60),
    _country_code varchar(3),
    PRIMARY KEY(_country_name)
);

CREATE TABLE dbs_p.co2_emission (
	_year INTEGER,
    _amount DECIMAL(12,1),
    _country_name varchar(60),
    FOREIGN KEY(_country_name) REFERENCES dbs_p.country(_country_name) ON DELETE CASCADE,
    PRIMARY KEY(_year, _country_name)
);

CREATE TABLE dbs_p.population (
	_year INTEGER,
    _annual_growth DECIMAL(13,11),
    _total_count BIGINT,
    _country_name varchar(60),
    FOREIGN KEY(_country_name) REFERENCES dbs_p.country(_country_name) ON DELETE CASCADE,
    PRIMARY KEY(_year, _country_name)
);

CREATE TABLE dbs_p.gdp (
	_country_name varchar(60),
	_year INTEGER,
    _value DECIMAL(27,7),
	FOREIGN KEY(_country_name) REFERENCES dbs_p.country(_country_name) ON DELETE CASCADE,
    PRIMARY KEY(_year, _country_name)
);

CREATE TABLE dbs_p.life_info (
	_year INTEGER,
    _status varchar(15),
    _life_expectancy DECIMAL(3,1),
    _country_name varchar(60),
    FOREIGN KEY(_country_name) REFERENCES dbs_p.country(_country_name) ON DELETE CASCADE,
    PRIMARY KEY(_year, _country_name)
);
