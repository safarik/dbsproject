from logging import debug
from dash_core_components.Slider import Slider
from dash_html_components.Br import Br
from dash_html_components.Div import Div
import numpy as np
from numpy.core.fromnumeric import size
from numpy.lib.histograms import histogram
from pandas.core.indexes import multi
import psycopg2 as pg
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import pandas as pd
import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots

# Datenbank Zugangsdaten
DB_HOST = "localhost"
DB_NAME = "postgres"
DB_USER = "postgres"
DB_PASS = "connectorZX007"

# app
app = dash.Dash(__name__)

# Verbindung zur Datenbank mit psycopg2-Bibliothek
conn = pg.connect(dbname=DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST)

cur = conn.cursor()

# Tabellen aus der Datenbank mit pandas-Bibliothek über SQL-Queries zu Dataframes umwandeln
df = pd.read_sql(
    "SELECT * FROM dbs_p.population", conn)

df_2 = pd.read_sql("SELECT * FROM dbs_p.gdp", conn)

df_3 = pd.read_sql(
    "SELECT * FROM (dbs_p.gdp NATURAL JOIN dbs_p.life_info) NATURAL JOIN dbs_p.population", conn)

df_3["gdppc"] = df_3["_value"]/df_3["_total_count"]

df_4 = df_3

df_5 = pd.read_sql(
    "SELECT * FROM (dbs_p.co2_emission NATURAL JOIN dbs_p.gdp) NATURAL JOIN dbs_p.population", conn)
df_5["gdppc"] = df_5["_value"]/df_5["_total_count"]

conn.commit()

cur.close()

conn.close()


# App-Layout: So werden die Visualisierung in der Anwendung angeordnet
app.layout = html.Div(className='row', children=[
    # Überschrift
    html.H1(children='How does the gdp affect a country regarding the life expectancy and the emission of CO2?',
            style={'text-align': 'center'}),
    # Dropdown, im Grunde eine Suchleiste, um nach Ländern zu filtrieren
    dcc.Dropdown(id='loc-dropdown',
                 options=[{'label': i, 'value': i}
                          for i in df['_country_name'].unique()],
                 value=['Germany'],
                 multi=True,
                 style={'width': "40%"}),

    html.Div(children=[
        dcc.Graph(id='total-count',
                  style={'display': 'inline-block'}, figure={'layout': {'width': 900}}),
        dcc.Graph(
            id='gdp-evo', style={'display': 'inline-block'}, figure={'layout': {'width': 900}})
    ]),

    html.Div(children=[
        dcc.Graph(id='lm',
                  style={'display': 'inline-block'}, figure={'layout': {'width': 900}}),
        dcc.Graph(
            id='pie-chart', style={'display': 'inline-block'}, figure={'layout': {'width': 900}})
    ]),
    html.Br(),

    # Lineare Regression gesamt
    dcc.Graph(id='lm-2'),

    # Slider, der eine Reichweite wiederspiegelt, um Grafik in x-Achse einzugrenzen
    dcc.RangeSlider(
        id='gdp-slider',
        min=0,
        max=120000,
        step=5000,
        value=[0, 120000],
        marks={
            0: {'label': '0', 'style': {'color': '#3B3EFF'}},
            15000: {'label': '15K', 'style': {'color': '#000000'}},
            30000: {'label': '30K', 'style': {'color': '#000000'}},
            45000: {'label': '45K', 'style': {'color': '#000000'}},
            60000: {'label': '60K', 'style': {'color': '#000000'}},
            75000: {'label': '75K', 'style': {'color': '#000000'}},
            90000: {'label': '90K', 'style': {'color': '#000000'}},
            105000: {'label': '105K', 'style': {'color': '#000000'}},
            120000: {'label': '120K', 'style': {'color': '#f50'}},
        }
    ),
    html.Br(),

    html.Div([
        dcc.Graph(id='scatter-plot')],
        style={"width": "43%", "display": "inline-block"}),
    html.Div([
        dcc.Graph(id='life-bar')],
        style={"width": "45%", "display": "inline-block"}),
    html.Div([
        html.Div([
            dcc.Slider(
                id='year-slider',
                min=1961,
                max=2017,
                step=1,
                value=1980,
                marks={1961: {'label': '1961', 'style': {'color': '#000000'}},
                       1970: {'label': '1970', 'style': {'color': '#000000'}},
                       1980: {'label': '1980', 'style': {'color': '#000000'}},
                       1990: {'label': '1990', 'style': {'color': '#000000'}},
                       2000: {'label': '2000', 'style': {'color': '#000000'}},
                       2010: {'label': '2010', 'style': {'color': '#000000'}},
                       2017: {'label': '2017', 'style': {'color': '#000000'}}},
                included=False,
                vertical=True)], style={"height": "420px"}),
    ],
        style={"width": "2%", "height": "100%", "display": "inline-block"}),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br()
])

# Interaktiver Teil, Input durch den Benutzer der Anwendung


@ app.callback(
    [Output(component_id='total-count', component_property='figure'),
     Output(component_id='gdp-evo', component_property='figure'),
     Output(component_id='lm', component_property='figure'),
     Output(component_id='pie-chart', component_property='figure'),
     Output(component_id='lm-2', component_property='figure'),
     Output(component_id='scatter-plot', component_property='figure'),
     Output(component_id='life-bar', component_property='figure')],
    [Input(component_id='loc-dropdown', component_property='value'),
     Input(component_id='gdp-slider', component_property='value'),
     Input(component_id='year-slider', component_property='value')]
)
# Die Variablen, die durch den Nutzer generiert werden, filtern die Dataframes, sodass quasi neue Tabellen entstehen
def update_graph(selected_location, value_range, selected_year):
    filtered_df = df[df['_country_name'].isin(selected_location)]
    filtered_df_2 = df_2[df_2['_country_name'].isin(selected_location)]
    filtered_df_3 = df_3[df_3['_country_name'].isin(selected_location)]
    filtered_df_4 = df_4[(value_range[0] <= df_4['gdppc'])
                         & (df_4['gdppc'] <= value_range[1])]

    # diese Interaktion verlangt Kommunikation mit Datenbank im Callback
    conn = pg.connect(dbname=DB_NAME, user=DB_USER,
                      password=DB_PASS, host=DB_HOST)

    cur = conn.cursor()

    df_6 = pd.read_sql(
        f"SELECT * FROM (dbs_p.co2_emission NATURAL JOIN dbs_p.gdp) NATURAL JOIN dbs_p.population WHERE _year = {selected_year}", conn)
    df_6["gdppc"] = df_6["_value"]/df_6["_total_count"]
    df_6["epc"] = df_6["_amount"]/df_6["_total_count"]
    filtered_df_5 = df_6[(value_range[0] <= df_6['gdppc'])
                         & (df_6['gdppc'] <= value_range[1])]

    df_7 = pd.read_sql(
        f"SELECT * FROM ((dbs_p.co2_emission NATURAL JOIN dbs_p.gdp) NATURAL JOIN dbs_p.population) NATURAL JOIN dbs_p.life_info WHERE _year = {selected_year}", conn)
    df_7["epc"] = df_7["_amount"]/df_7["_total_count"]
    filtered_df_7 = df_7[df_7['_country_name'].isin(selected_location)]

    conn.commit()

    cur.close()

    conn.close()

    # Grafiken werden basierend auf den gefilterten Daten, die nun Dataframes sind, erstellt

    line_fig = px.line(filtered_df,
                       x='_year', y='_total_count',
                       color='_country_name',
                       labels={"_year": "year",
                               "_total_count": "total population",
                               "_country_name": "country"},
                       title='Population')

    line_fig_2 = px.line(filtered_df_2,
                         x='_year', y='_value',
                         color='_country_name',
                         labels={"_year": "year",
                                 "_value": "GDP",
                                 "_country_name": "country"},
                         title='Gross domestic product')

    lm_fig = px.scatter(filtered_df_3,
                        x='gdppc', y='_life_expectancy', trendline='ols',
                        labels={"gdppc": "GDP value per capita",
                                "_life_expectancy": "life expectancy",
                                "_status": "country status"},
                        title=f'Linear model to compare countries: {selected_location}')

    pie_fig = px.pie(filtered_df_3, values='gdppc', names='_country_name',
                     labels={'gdppc': 'sum of gdppc',
                             '_country_name': 'country'},
                     title=f'Pie chart to compare GDP per capita of listed countries')
    lm_fig_2 = px.scatter(filtered_df_4,
                          x='gdppc', y='_life_expectancy', trendline='ols', facet_col='_status',
                          labels={"gdppc": "GDP value per capita",
                                  "_life_expectancy": "life expectancy",
                                  "_status": "country status"},
                          title=f'Linear model for all countries')

    scatter_fig = px.scatter(filtered_df_5, x="gdppc",
                             y="epc", hover_data=['_country_name'], color='_annual_growth',
                             labels={"gdppc": "GDP value",
                                     "epc": "CO2 emission in tonnes",
                                     "_year": "year",
                                     '_annual_growth': 'population growth',
                                     '_country_name': 'country'},
                             title=f'Scatterplot linking CO2 emission to GDP (both per capita)')

    barchart_fig = px.bar(
        filtered_df_7, x='_country_name', y='_life_expectancy', color='epc',
        labels={
            "epc": "EPC",
            "_year": "year",
            '_life_expectancy': 'life expectancy',
            '_country_name': 'country'})

    # alle Grafiken werden zurückgegeben
    return line_fig, line_fig_2, lm_fig, pie_fig, lm_fig_2, scatter_fig, barchart_fig


# lokalen Server starten
if __name__ == '__main__':
    app.run_server(debug=True)
