import pandas as pd
import psycopg2
import numpy as np


def getemissionframe():
    df_emission = pd.read_csv('co2_emission_clean.csv', encoding='utf-8')

    df_emission.rename(columns={'Entity': 'Country'}, inplace=True)
    df_emission['Country'] = df_emission['Country'].astype('category')
    df_emission['Code'] = df_emission['Code'].astype('category')
    df_emission['Year'] = df_emission['Year'].astype('category')
    df_emission.rename(columns={'Annual CO2 emissions (tonnes )': 'Amount'}, inplace=True)
    df_emission['Amount'] = df_emission['Amount'].astype('category')
    return pd.DataFrame(df_emission, columns=['Country', 'Code', 'Year', 'Amount'])


def getpopulationframe():
    df_total = pd.read_csv('population_total_clean.csv', encoding='utf-8')
    df_total.rename(columns={'Country Name': 'Country'}, inplace=True)
    df_total['Country'] = df_total['Country'].astype('category')
    df_total['Year'] = df_total['Year'].astype('Int64')
    df_total.rename(columns={'Count': 'Total_count'}, inplace=True)

    df_growth = pd.read_csv('population_growth_clean.csv', encoding='utf-8')
    df_growth = df_growth.drop(['Country Code', 'Indicator Name', 'Indicator Code'], axis=1)
    df_growth_help = df_growth.drop(['Country Name'], axis=1)
    df_growth.rename(columns={'Country Name': 'Country'}, inplace=True)
    df_growth['Country'] = df_growth['Country'].astype('category')
    df_growth = pd.melt(df_growth, id_vars=['Country'], value_vars=df_growth_help,
                        var_name='Year', value_name='Growth')

    df_growth['Year'] = df_growth['Year'].astype('float')
    df_growth['Year'] = df_growth['Year'].astype('Int64')
    return pd.merge(df_total, df_growth, on=['Country', 'Year'], how='left')


def getgdpframe():
    df_gdp = pd.read_csv('gdp_clean.csv', encoding='utf-8')
    df_gdp = df_gdp.drop(['Country Code', 'Indicator Name', 'Indicator Code'], axis=1)
    df_gdp_help = df_gdp.drop(['Country Name'], axis=1)
    df_gdp = pd.melt(df_gdp, id_vars=['Country Name'], value_vars=df_gdp_help, var_name='Year', value_name='GDP')
    df_gdp.rename(columns={'Country Name': 'Country'}, inplace=True)
    df_gdp['Country'] = df_gdp['Country'].astype('category')
    df_gdp['Year'] = df_gdp['Year'].astype('category')
    df_gdp['GDP'] = df_gdp['GDP'].astype('category')
    return pd.DataFrame(df_gdp, columns=['Country', 'Year', 'GDP'])


def getlifeframe():
    df_life = pd.read_csv('life_expectancy_clean.csv', encoding='utf-8')
    df_life = df_life[['Country', 'Year', 'Status', 'Life expectancy']]
    df_life['Country'] = df_life['Country'].astype('category')
    df_life['Year'] = df_life['Year'].astype('category')
    df_life['Status'] = df_life['Status'].astype('category')
    df_life.rename(columns={'Life expectancy': 'Life_expectancy'}, inplace=True)
    df_life['Life_expectancy'] = df_life['Life_expectancy'].astype('category')
    return pd.DataFrame(df_life, columns=['Country', 'Year', 'Status', 'Life_expectancy'])


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    pd.options.display.width = 0
    pd.options.display.max_rows = 10000
    pd.options.display.max_info_columns = 10000

# Data for co2 emissions
data_emission = getemissionframe()
data_emission = data_emission.dropna()

# Data for population
df_population = getpopulationframe()
data_population = pd.DataFrame(df_population, columns=['Country', 'Year', 'Total_count', 'Growth'])

# Data for GDP
data_gdp = getgdpframe()

# Data for Life Expectancy
data_life = getlifeframe()

df_country = pd.read_csv('gdp_clean.csv')
df_country = df_country[['Country Name', 'Country Code']]
data_country = pd.DataFrame(df_country, columns=['Country Name', 'Country Code'])
data_country.rename(columns={'Country Name': 'Country'}, inplace=True)
data_country.rename(columns={'Country Code': 'Code'}, inplace=True)

data_country = data_country[data_country['Country'].isin(data_life['Country'])]
data_gdp = data_gdp[data_gdp['Country'].isin(data_country['Country'])]
data_population = data_population[data_population['Country'].isin(data_country['Country'])]
data_emission = data_emission[data_emission['Country'].isin(data_country['Country'])]

print("All DataFrames ready")
conn = None
try:
    # connect to the PostgreSQL server
    print('Connecting to the PostgreSQL database...')
    conn = psycopg2.connect(host="localhost", database="co2", user="postgres", password="postgres")

    # create a cursor
    cur = conn.cursor()

    # execute a statement
    print('PostgreSQL database version:')
    cur.execute('SELECT version()')

    # display the PostgreSQL database server version
    db_version = cur.fetchone()
    print(db_version)

    # create schema
    cur.execute('DROP SCHEMA IF EXISTS dbs_p CASCADE')
    cur.execute('CREATE SCHEMA dbs_p')

    cur.execute('DROP TABLE IF EXISTS dbs_p.country CASCADE')
    cur.execute('CREATE TABLE dbs_p.country (Country VARCHAR(60) PRIMARY KEY, Code VARCHAR(10))')

    for row in data_country.itertuples():
        cur.execute(''' INSERT INTO dbs_p.country (Country, Code) 
                        VALUES (''' '\'' + row.Country + '\',' + '\'' + row.Code + '\')')

    # create table for population
    cur.execute('DROP TABLE IF EXISTS dbs_p.population')
    cur.execute('CREATE TABLE dbs_p.population (Country VARCHAR(60) , Year integer, Total_count float, Growth float,'
                'FOREIGN KEY(Country) REFERENCES dbs_p.country ON DELETE CASCADE, PRIMARY KEY (Country, Year))')

    # insert population data
    for row in data_population.itertuples():
        x = "\'" + row.Country + "\'"
        if row.Country == "Congo. Dem. Rep.":
            print(row)
        y = "\'" + str(row.Year) + "\'"
        z = "\'" + str(row.Total_count) + "\'"
        n = "\'" + str(row.Growth) + "\'"
        cur.execute('''
                    INSERT INTO dbs_p.population (Country, Year, Total_count, Growth)
                    VALUES (''' + x + ',' + y + ',' + z + ',' + n + ')')

    # create table for emissions
    cur.execute('DROP TABLE IF EXISTS dbs_p.emission')
    cur.execute('CREATE TABLE dbs_p.emission (Country VARCHAR(60), Code VARCHAR(10), Year integer, Amount float, '
                'FOREIGN KEY(Country) REFERENCES dbs_p.country ON DELETE CASCADE, PRIMARY KEY (Year, Country))')

    # insert emission data
    for row in data_emission.itertuples():
        x = "\'" + row.Country + "\'"
        y = "\'" + row.Code + "\'"
        z = "\'" + str(row.Year) + "\'"
        n = "\'" + str(row.Amount) + "\'"
        cur.execute('''
                    INSERT INTO dbs_p.emission (Country, Code, Year, Amount)
                    VALUES (''' + x + ',' + y + ',' + z + ',' + n + ')')

    # create table for life expectancy
    cur.execute('DROP TABLE IF EXISTS dbs_p.life_expectancy')
    cur.execute('CREATE TABLE dbs_p.life_expectancy (Country VARCHAR(60), Year integer, Status VARCHAR(60), '
                'Life_expectancy float, FOREIGN KEY(Country) REFERENCES dbs_p.country ON DELETE CASCADE, '
                'PRIMARY KEY (Country, Year))')

    # insert life data
    for row in data_life.itertuples():
        x = "\'" + row.Country + "\'"
        y = "\'" + str(row.Year) + "\'"
        z = "\'" + row.Status + "\'"
        n = "\'" + str(row.Life_expectancy) + "\'"
        cur.execute('''
                        INSERT INTO dbs_p.life_expectancy (Country, Year, Status, Life_expectancy)
                        VALUES (''' + x + ',' + y + ',' + z + ',' + n + ')')

    # save data
    conn.commit()
    print('Done')
    # close the communication with the PostgreSQL
    cur.close()
except (Exception, psycopg2.DatabaseError) as error:
    print(error)
finally:
    if conn is not None:
        conn.close()
        print('Database connection closed.')


