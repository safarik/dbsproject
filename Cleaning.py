# DBS_Assignment Excercise 2 / Datacleaning 

# import packages
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib

# Data Cleaning Step 1: drop data/columns
data_new=data.drop(columns=data.columns[4:22], axis=1)
data_new.to_csv('life_expectancy_clean.csv', index=False)

# Data Cleaning Step 2: unify country names
data1 = pd.read_csv("population_growth.csv")
data1['Country Name']= data1['Country Name'].replace({'^Venezuela.*': 'Venezuela'}, regex=True)
data1['Country Name']= data1['Country Name'].replace({'^Micronesia.*': 'Micronesia'}, regex=True)
data1['Country Name']= data1['Country Name'].replace({'^Iran.*': 'Iran'}, regex=True)
data1['Country Name']= data1['Country Name'].replace({'^Bahamas.*': 'Bahamas'}, regex=True)
data1['Country Name']= data1['Country Name'].replace({'^Egypt.*': 'Egypt'}, regex=True)
data1['Country Name']= data1['Country Name'].replace({'^Gambia.*': 'Gambia'}, regex=True)
data1['Country Name']= data1['Country Name'].replace({'^Hong.*': 'Hong Kong'}, regex=True)
data1['Country Name']= data1['Country Name'].replace({'^Macao.*': 'Macao'}, regex=True)
data1['Country Name']= data1['Country Name'].replace({'^Yemen.*': 'Yemen'}, regex=True)
data1['Country Name']= data1['Country Name'].replace({'^.*People.*$': 'North Korea'}, regex=True)
data1['Country Name']= data1['Country Name'].replace({'Korea.*Rep|Rep.*Korea': 'South Korea'}, regex=True)
data1['Country Name'].iloc[data1['Country Code'] == 'COD'] = 'Congo Dem. Rep.'
data1['Country Name'].iloc[data1['Country Code'] == 'COG'] = 'Congo Rep.'
data1.to_csv("population_growth_cleaner.csv", index=False)

# Data Cleaning Step 3: delete entries without Country-Code (includes doupled entry of Kyrgysztan)
data2 = pd.read_csv("co2_emission.csv")
nan_value = float("NaN")
data2.replace("", nan_value, inplace=True)
data2 = data2.dropna(subset=['Code'])
data2.to_csv("co2_emission_clean.csv", index=False)